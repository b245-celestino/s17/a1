/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function getInformation() {
  let fullname = prompt("Enter your Full Name :");
  let age = prompt("Enter your age:");
  let location = prompt("Enter your address:");

  console.log("Hello," + fullname);
  console.log("You are " + age + " years old.");
  console.log("You live in " + location);
}

getInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:

let showBands = function topFiveBands() {
  let firstBand = "1. Fugazi";
  let secondBand = "2. The Beatles";
  let thirdBand = "3. Metallica";
  let fourthBand = "4. The Eagles";
  let fifthBand = "5. Eraserheads";
  console.log(firstBand);
  console.log(secondBand);
  console.log(thirdBand);
  console.log(fourthBand);
  console.log(fifthBand);
};
showBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:

let showMovies = function showTopMovies() {
  let firstMovie = "97%";
  let secondMovie = "96%";
  let thirdMovie = "91%";
  let fourthMovie = "93%";
  let fifthMovie = "96%";
  console.log("1. The Godfather ");
  console.log("Rotten Tomatoes Rating: " + firstMovie);
  console.log("2. The Godfather, Part II ");
  console.log("Rotten Tomatoes Rating: " + secondMovie);
  console.log("3. Shawshank Redemption");
  console.log("Rotten Tomatoes Rating: " + thirdMovie);
  console.log("4. To Kill A Mockingbird ");
  console.log("Rotten Tomatoes Rating: " + fourthMovie);
  console.log("5 Psycho ");
  console.log("Rotten Tomatoes Rating: " + fifthMovie);
};
showMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();

// console.log(friend1);
// console.log(friend2);
